'*********************************************************************************************************************
'
'                                                    Script to write batch file to 
'                                          generate historical feed files (1 per day for x years)
'
'********************************************************************************************************************* 
Option Explicit

'**** Variables ****

Dim sDate, eDate, copyDate, cmdline, cmdline2, cmdline3, cmdline4, cmdline5, cmdline6, cmdline7, WshShell
Dim sMonth, sDay, sYear, lYear, CurrDate, MyArray, MyArray1, MyArray2, BatchFile, oFSO, oFSO_File, CrLF  


Const ForAppending = 8

CrLF = Chr(13) & Chr(10)

'** Create Batch File 
BatchFile = "O:\Prodman\Dev\WFI\Feeds\RedIntelligence\RedIntelligence.bat"
Set oFSO = CreateObject("Scripting.FileSystemObject")
Set oFSO_File = oFSO.OpenTextFile(BatchFile, ForAppending, True)
  
  
 
'-- Create Date Function of current date  
 if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  'sYear = Year(Now)
  lYear = mid(Year(Now),1,4)
  sYear = mid(Year(Now),3,4)
	
  'sDate = sYear & sMonth & sDay
'MsgBox sdate

CurrDate = lYear & "-" & sMonth & "-" & sDay 
  
  
  
  
  
'** Set Date Variables
sDate = "2015-12-15" 
eDate = "2015-12-16"
copyDate = "2015-12-15"



'** Loop to write batch file
Do While copyDate<=CurrDate
'cmdline = "O:\AUTO\Apps\ifeed\v01\iFeedV01.jar -m -s HAT_MY_Diesel -p ""set @fdate ='"& sDate &"'; set @tdate ='"& eDate &"'"" -fdt ""select '" &(Replace(sDate,"-","")) &"'"" -hdt ""select '" &(Replace(sDate,"-","")) &"'"" -fsx ""select '_Act'"" -f o:\prodman\dev\wca\worldquant\695_act.sql -d n:\no_cull_feeds\byyear\695\worldquant\695act\"
'cmdline = "O:\AUTO\Apps\ifeed\v01\iFeedV01.jar -m -s HAT_MY_Diesel -p ""set @fdate ='"& sDate &"'; set @tdate ='"& eDate &"'"" -fdt ""select '" &(Replace(sDate,"-","")) &"'"" -hdt ""select '" &(Replace(sDate,"-","")) &"'"" -fsx ""select '_Act'"" -f o:\prodman\dev\wca\worldquant\689_act.sql -d n:\no_cull_feeds\byyear\695\worldquant\689act\"
'cmdline = "O:\AUTO\Apps\ifeed\v01\iFeedV01.jar -m -s HAT_MY_Diesel -p ""set @fdate ='"& sDate &"'; set @tdate ='"& eDate &"'"" -fdt ""select '" &(Replace(sDate,"-","")) &"'"" -hdt ""select '" &(Replace(sDate,"-","")) &"'"" -fsx ""select '_Ann'"" -f o:\prodman\dev\wca\worldquant\695_ann.sql -d n:\no_cull_feeds\byyear\695\worldquant\695ann\"

'MsgBox "EDI_BondSigma_" &copyDate& ".zip"

cmdline = "O:\AUTO\Scripts\vbs\FixedIncome\CopyFileOnly.vbs N:/No_Cull_Feeds/calcoutfull/ O:/Prodman/Dev/WFI/Feeds/RedIntelligence/Archive/  EDI_BondSigma_" &copyDate& ".zip"

cmdline2 = "O:\AUTO\Scripts\vbs\FixedIncome\Unzipper.vbs O:\Prodman\Dev\WFI\Feeds\RedIntelligence\Archive\ EDI_BondSigma_ " &copyDate& " O:\Prodman\Dev\WFI\Feeds\RedIntelligence\Archive\"

cmdline3 = "O:\Prodman\Dev\WFI\Feeds\RedIntelligence\ReddIntel_BondSigma_CalcOut_Load.vbs HAT_MY_Diesel"

cmdline4 = "O:\AUTO\Apps\ifeed\v01\iFeedV01.jar -m -s HAT_MY_Diesel -p ""set @fdate ='"& sDate &"'; set @tdate ='"& eDate &"'"" -fdt ""select '" &(Replace(sDate,"-","")) &"'"" -hdt ""select '" &(Replace(sDate,"-","")) &"'"" -f O:\Prodman\Dev\WFI\Feeds\RedIntelligence\BondSigma.sql -d O:\Prodman\Dev\WFI\Feeds\RedIntelligence\Output\"

cmdline5 = "O:\AUTO\Scripts\vbs\FileTidy.vbs O:\Prodman\Dev\WFI\Feeds\RedIntelligence\Archive\ zip"

cmdline6 = "O:\AUTO\Scripts\vbs\FileTidy.vbs O:\Prodman\Dev\WFI\Feeds\RedIntelligence\Archive\ csv"

oFSO_File.Write cmdline &CrLF &cmdline2 &CrLF &cmdline3 &CrLF &cmdline4 &CrLF &cmdline5 &CrLF &cmdline6 &CrLF &CrLF
sDate = DateAdd("d",1,sDate)
eDate = DateAdd("d",1,eDate)
copyDate = DateAdd("d",1,copyDate)
'MsgBox sDate
'MsgBox eDate
Call formatdate

Loop



Sub formatdate()
MyArray=Split(sDate,"/")
MyArray1=Split(eDate,"/")
MyArray2=Split(copyDate,"/")
sDate=Mid(MyArray(2),1,4) & "-" & Mid(MyArray(1),1,2) & "-" & Mid(MyArray(0),1,2)
'MsgBox sdate
eDate=Mid(MyArray1(2),1,4) & "-" & Mid(MyArray1(1),1,2) & "-" & Mid(MyArray1(0),1,2)
'MsgBox eDate
copyDate=Mid(MyArray2(2),1,4) & "-" & Mid(MyArray2(1),1,2) & "-" & Mid(MyArray2(0),1,2)
'MsgBox eDate
End Sub

cmdline7 = "O:\AUTO\Scripts\vbs\FixedIncome\Zipper.vbs 92 none"

oFSO_File.Write cmdline7 